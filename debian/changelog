fwknop (2.6.11-2) unstable; urgency=medium

  * Enable NFQ in default config file (LP: #2076224)
  * Remove unnecessary libpcap0.8-dev build dep
  * Bump Standards-Version up to 4.7.0

 -- Francois Marier <francois@debian.org>  Sat, 10 Aug 2024 14:29:05 -0700

fwknop (2.6.11-1) unstable; urgency=medium

  * New upstream release
    - drop all patches merged upstream (most of them)
  * Bump copyright year in debian/copyright
  * Remove redundant dpkg-dev line in Build-Depends

 -- Francois Marier <francois@debian.org>  Fri, 03 May 2024 19:57:57 -0700

fwknop (2.6.10-20.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Upload to unstable. (Closes: #1061958)

 -- Michael Hudson-Doyle <michael.hudson@ubuntu.com>  Fri, 01 Mar 2024 10:06:45 +1300

fwknop (2.6.10-20.1) experimental; urgency=medium

  [ Lukas Märdian ]
  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.
  * Account for additional t64 Breaks/Replaces (Closes: #1062046).

  [ Francois Marier ]
  * Add "pcap capture not compiled in" to logcheck rules.

 -- Michael Hudson-Doyle <mwhudson@debian.org>  Thu, 29 Feb 2024 04:44:10 +0000

fwknop (2.6.10-20) unstable; urgency=medium

  * Add debian/NEWS mentioning the required NFQ configuration change.
  * Add missing /etc/gai.conf to apparmor profile.

 -- Francois Marier <francois@debian.org>  Mon, 13 Nov 2023 15:42:27 -0800

fwknop (2.6.10-19) unstable; urgency=medium

  * Move apparmor profile back into fwknop-apparmor-profile.
  * Add missing apparmor-profiles-extra dependency (closes: #1055670).
  * Add Breaks/Replaces on fwknop-server for fwknop-apparmor-profile.
  * Switch to NFQ (closes: #949331).

 -- Francois Marier <francois@debian.org>  Mon, 13 Nov 2023 10:48:47 -0800

fwknop (2.6.10-18) unstable; urgency=medium

  * Add missing Replaces for fwknop-apparmor-profile.

 -- Francois Marier <francois@debian.org>  Sat, 09 Sep 2023 12:23:48 -0700

fwknop (2.6.10-17) unstable; urgency=medium

  * Add generated files to debian/clean and debian/source/options
    (closes: #1045067).
  * Move apparmor file into fwknop-server and install it in /etc/apparmor.d
    directly (as discussed in #1034055).
  * Drop obsolete lsb-base dependency.

 -- Francois Marier <francois@debian.org>  Sat, 09 Sep 2023 11:34:15 -0700

fwknop (2.6.10-16) unstable; urgency=high

  * Install apparmor profile in /usr/share/apparmor/extra-profiles/
    instead of the systemd service directory. Note that the profile
    will not be used unless manually copied into /etc/apparmor.d/
    (Closes: #1034055).

 -- Francois Marier <francois@debian.org>  Mon, 10 Apr 2023 20:52:01 -0700

fwknop (2.6.10-15) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: (Closes: #1028394)
    + Add missing B-D: perl-xs-dev for building a perl extension.
    + Export a suitable PERL5LIB.

  [ Francois Marier ]
  * Bump Standards-Version up to 4.6.2.
  * Bump copyright year in debian/copyright.

 -- Francois Marier <francois@debian.org>  Tue, 10 Jan 2023 21:23:46 -0800

fwknop (2.6.10-14) unstable; urgency=medium

  * Remove outdated gpgme.m4 from build (closes: #1024481).
  * Remove obsolete @setcontentsaftertitlepage command from libfko.texi.
  * Update lintian overrides (new warning text).
  * libfko-perl: Drop Multi-Arch: same.
  * Drop ancient debian/NEWS file.
  * Bump Standards-Version up to 4.6.1.
  * Bump copyright year in debian/copyright.

 -- Francois Marier <francois@debian.org>  Sat, 26 Nov 2022 17:41:38 -0800

fwknop (2.6.10-13) unstable; urgency=medium

  * Fix AppArmor profile for recent versions of Debian.
  * Ship the AppArmor file in /usr/ instead of /etc/.
  * Mark patches as forwarded upstream.
  * Bump Standards-Version up to 4.6.0.

 -- Francois Marier <francois@debian.org>  Sun, 12 Sep 2021 20:09:33 -0700

fwknop (2.6.10-12) unstable; urgency=medium

  * Bump Standards-Version up to 4.5.1.
  * Bump debian/watch up to version 4.
  * Bump copyright year in debian/copyright.
  * Add upstream metadata file.

 -- Francois Marier <francois@debian.org>  Mon, 18 Jan 2021 22:55:24 -0800

fwknop (2.6.10-11) unstable; urgency=high

  * Fix gcc10 build (closes: #957244).
  * Fix typo in lintian override.
  * Build with root explicitly.

 -- Francois Marier <francois@debian.org>  Wed, 22 Jul 2020 20:33:25 -0700

fwknop (2.6.10-10) unstable; urgency=medium

  * Remove PIDFile from systemd service since systemd tries to read that
    file before fwknopd has written to it.

 -- Francois Marier <francois@debian.org>  Mon, 15 Jun 2020 20:16:27 -0700

fwknop (2.6.10-9) unstable; urgency=medium

  * Add ipset support to AppArmor profile (closes: #949332).
  * Add Ubuntu 20.04 support to AppArmor profile (LP: #1880009).
  * Bump debian-compat to 13.
    - Update location of files in *.install and *.manpages.
    - Add libfko.so symlink and libfko.la to debian/not-installed.

 -- Francois Marier <francois@debian.org>  Sun, 14 Jun 2020 18:14:42 -0700

fwknop (2.6.10-8) unstable; urgency=medium

  * Mark libfko3-dev as "Multi-Arch: same".

 -- Francois Marier <francois@debian.org>  Sat, 22 Feb 2020 09:30:09 -0800

fwknop (2.6.10-7) unstable; urgency=medium

  * Revert NFQ capture support since it disables pcap capture.

 -- Francois Marier <francois@debian.org>  Tue, 18 Feb 2020 08:08:43 -0800

fwknop (2.6.10-6) unstable; urgency=medium

  * Add missing "Wants" directive in systemd service file (closes: #949323)
  * Enable support for NFQ capture (closes: #949331)
  * Bump Standards-Version up to 4.5.0.
  * Replace debian/compat with debhelper-compat (= 12).

 -- Francois Marier <francois@debian.org>  Sat, 15 Feb 2020 11:38:27 -0800

fwknop (2.6.10-5) unstable; urgency=medium

  * Stop building libfko-python since it's Python2-only (closes: #936575).

 -- Francois Marier <francois@debian.org>  Thu, 12 Sep 2019 10:32:19 -0700

fwknop (2.6.10-4) unstable; urgency=medium

  * Patch /run directory handling.
  * Fix typo in Restart directive of systemd service file.
  * Bump Standards-Version to 4.4.0.

 -- Francois Marier <francois@debian.org>  Mon, 08 Jul 2019 13:20:10 -0700

fwknop (2.6.10-3) unstable; urgency=medium

  * Restart fwknop-server on failure (closes: #927392)
  * Make libfko3 and libfko-perl "Multi-Arch: same" (Multiarch hinter)

 -- Francois Marier <francois@debian.org>  Sat, 06 Jul 2019 14:34:40 -0700

fwknop (2.6.10-2) unstable; urgency=medium

  * Remove all debconf leftovers (closes: #581635)

 -- Francois Marier <francois@debian.org>  Thu, 28 Feb 2019 10:37:06 -0800

fwknop (2.6.10-1) unstable; urgency=medium

  * New upstream release (closes: #923086)
    - new `--server-resolve-ipv4` option
    - drop all patches (merged upstream)
  * Patch AppArmor profile (closes: #911485).
  * Adopt this package (closes: #831273).
  * Add myself to debian/copyright.
  * Move repo to salsa.debian.org.

  * Add a build-depend on wget (closes: #840565).
  * Mention START_DAEMON (closes: #844016).
  * Move state files to /run/fwknop (closes: #911483).
  * Cleanup old files in /var/fwknop and remove 1.9.x cleanups.
  * Update postrm file to cleanup /run/fwknop
  * Add a logcheck rule for fwknop-server.
  * Include upstream systemd unit files.

  * Bump Standards-Version to 4.3.0.
  * Bump debhelper compat to 12.
  * Use HTTPS URL in debian/copyright.
  * Add missing people and years to debian/copyright.
  * Use HTTPS URL for upstream homepage.
  * USe HTTPS URL in debian/watch.
  * Update source link to GitHub.
  * Run wrap_and_sort -ast.
  * Remove unnecessary X-Python{,3}-Version field in debian/control.
  * Trim trailing whitespace.
  * Change priority extra to priority optional.
  * Use /usr/share/dpkg/architecture.mk in debian/rules.
  * Remove obsolete dbgsym-migration line in debian/rules.
  * Mark the -doc package as "Multi-Arch: foreign".

 -- Francois Marier <francois@debian.org>  Mon, 25 Feb 2019 14:56:20 -0800

fwknop (2.6.9-2) unstable; urgency=medium

  * QA upload.
  * Drop upstart system job.
  * s/libcap/libpcap/ in long descriptions. Closes: #836890.
  * debhelper compat 10.
  * Don't activate triggers twice.

 -- Adam Borowski <kilobyte@angband.pl>  Thu, 05 Oct 2017 20:36:06 +0200

fwknop (2.6.9-1) unstable; urgency=medium

  * QA upload.
  * New upstream release
    - Closes: #826718 - Please update fwknop
    - Patches:
      + Updated spelling.diff patch
      + Added new patch 002_fkoversion.diff
    - Added debian/source/include-binaries files
    - libfko2* packages become libfko3* packages to follow shared library
      number change.
    - Updated debian/copyright accordingly and fixed some lintian warnings.
  * Bump up debian policy from 3.9.5 to 3.9.8: no changes
  * Removed debug package which is automatically built.
  * Removed ldconfig call from libfko maintainer scripts and added
    libfko3.triggers to replace them.
  * Fixed debian/control:
    - Use secure URI (https) for VCS field
    - Added dh-python to BD field
  * Added GPG signature verification to debian/watch file. Upstream GPG
    signature is stored in debian/upstream/signing-key.asc.

 -- Franck Joncourt <franck.joncourt@gmail.com>  Sat, 27 Aug 2016 22:09:29 +0200

fwknop (2.6.0-4) unstable; urgency=medium

  * QA upload.
  * debian/rules: Disable verbose dh mode.
  * Add --with-gpg=/usr/bin/gpg to configure call. Closes: #833465.
    This should probably not be required at all as we are using libgpgme,
    but at least it will not FTBFS and should behave as before.

 -- Santiago Vila <sanvila@debian.org>  Fri, 05 Aug 2016 00:29:16 +0200

fwknop (2.6.0-3) unstable; urgency=medium

  * QA upload.
  * Package is orphaned (Bug #831273). Set maintainer to "Debian QA Group".
  * Fix debian/rules for "dpkg-buildpackage -A" to work. Closes: #806027.
  * Fix spelling error in libfko2-dev short description.

 -- Santiago Vila <sanvila@debian.org>  Fri, 15 Jul 2016 15:05:36 +0100

fwknop (2.6.0-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Use "find -perm /x" instead of "find -perm +x". Closes: #808926

 -- Andreas Metzler <ametzler@debian.org>  Fri, 25 Dec 2015 14:24:41 +0100

fwknop (2.6.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "hardcodes /usr/lib/perl5":
    Use $Config{vendorarch} in debian/rules.
    (Closes: #752472)

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Aug 2014 17:58:22 +0200

fwknop (2.6.0-2) unstable; urgency=low

  * Moved installation of usr.sbin/fwknopd from d.rules to
    fwknop-apparmor-profile.install file. (Closes: #735766)
  * Added copyright stanza for android files in d.copyright
   (Closes: #735668).

 -- Franck Joncourt <franck@debian.org>  Sat, 18 Jan 2014 21:42:00 +0100

fwknop (2.6.0-1) unstable; urgency=low

  * Imported Upstream version 2.6.0

  * New fwknop-apparmor-profile package to provide the fwknop-server apparmor
    profile.
    - New entry in d.control to create the brand new binary package.
    - Added BD on dh-apparmor.
    - Updated d.rules to used dh_apparmor to manage developer scripts.
    - Added fwknop-apparmor-profile.dirs file to create apparmor directory at
      install.
    - Added fwknop-apparmor-profile as suggested package for the fwknop server
      in d.control.
  * Bump up debian policy from 3.9.4 to 3.9.5:
    - Removed dependencies on dpkg (>= 1.15.4) | install-info

 -- Franck Joncourt <franck@debian.org>  Mon, 13 Jan 2014 22:10:17 +0100

fwknop (2.5.1-1) unstable; urgency=low

  * Imported Upstream version 2.5.1

 -- Franck Joncourt <franck@debian.org>  Sun, 28 Jul 2013 19:05:04 +0200

fwknop (2.5-1) unstable; urgency=low

  * Imported Upstream version 2.5
    - libfko breaks the old ABI. It is bumped from 1.0.0 up to 2.0.0
      old packages libfko1* are renamed as libfko2*.
      Updated d.rules to make reference to the new packages.
    - Patch gpl2.0.diff applied upstream: Removed it from the patches
      directory.
    - New python binding

  * The new upstream release add a m4 file to the m4 directory.
    d.rules: assume the m4 directory is created ; no need to remove it in the
    clean target and no need to create it in the configure target anymore.
  * Fixed vcs fields in d.control to use the canonical URI.
    (Caught by lintian).
  * Fixed documentation spelling error (s/GNU Public/GNU General Public/g)
    through a new patch 0001_fix-doc.patch.
  * Removed useless depdencies default-mta | mail-transport-agent on
    fwknop-server in d.rules.
  * Used CFLAGS, CPPFLAGS and LDFLAGS variables to enable the hardening flags
    to build the libfko-perl binding.
  * Bumped up Standards-Version to 3.9.4 - no changes.
  * Packaged the libfko python binding:
    - d.control: Set X-Python-Version to >= 2.7 and added BD on python-all-dev
                 Defined the brand new libfko-python binding.
    - d.rules: Used dh_python2 debhelper
               Added new instances to build and install libfko-python

 -- Franck Joncourt <franck@debian.org>  Tue, 23 Jul 2013 21:08:42 +0200

fwknop (2.0.4-2) unstable; urgency=low

  * Fixed FTBS ./gpl-2.0.texi:20: raising the section level of @appendixsubsec
    which is too low (Closes: #708589):
    Added patch gpl-2.0.diff to substitute @appendixsubsec entries by
    @appendixsec.

 -- Franck Joncourt <franck@debian.org>  Sun, 19 May 2013 17:55:57 +0200

fwknop (2.0.4-1) unstable; urgency=low

  * Fixed bad permissions for the fwknopd.conf conf file (Closes: #689934)
    - d.rules: Set the permissions for the fwknopd.conf file to 600 and
      prevent dh_fixperms from adjusting them.
    - d.fwknop-server.lintian-overrides: Add an override entry to make sure
      lintian does not complain about the new permissions.
  * Added libfko-doc binary package:
    - d.libfko-doc.docs: Shipped html libfko manual.
    - d.libfko-doc.doc-base: Added doc-base entry for the manual.
    - d.rules: Built the html doc in the overwrite_dh_auto_build target.
  * Added libfko-perl binary package (Closes: #682028):
    - d.rules : Added overwrite_dh_auto_install target to install
      the Perl module.
    - d.control: added new entrey for the libfko-perl package.
  * Bumped up libfko soname because the ABI changed:
    - libfko0-* packages become libfko1-* package.
    - d.libfko.symbols: Updated list of symbols.
  * d.libfko1.links: Removed useless links.
  * Added libfko.so symlink for the dev package as noticed by lintian
    (dev-pkg-without-shlib-symlink).
    - Removed libfko1-dev.links file.
    - d.rules: Added new target overwrite_dh_link to link libfko.so.1.0.0
      to libfko.so.
  * Added upstart configuration file: fwknop-server.upstart

 -- Franck Joncourt <franck@debian.org>  Sun, 23 Dec 2012 17:57:55 +0100

fwknop (2.0.2-1) unstable; urgency=low

  * Imported Upstream version 2.0.2
  * Removed previous patches applied upstream.
  * Set debian compatibility to 9 in d.compat.
  * Fixed libfko0.symbols file which contains bad symbols from one of the
    first rc releases. The following functions have been removed:
    - _rijndael_decrypt
    - _rijndael_encrypt
    - append_b64
    - digest_to_hex
    - get_random_data
    - gpg_decrypt
    - gpg_encrypt
    - rij_salt_and_iv
    - rijndael_init
  * Added previously removed fileds in d.control (Vcs-git/Vcs-Browser)
    (Closes: #682057)
  * Created missing m4 directory before configuring the package.
  * Use of the hardening flags:
    - Added BD on dpkg-dev (>= 1.16.1~) and on debhelper (>= 9) in d.control.
    - Set debian compatibility to 9 in d.compat.
    - Set hardening flags through the DEB_BUILD_MAINT_OPTIONS variable,
      exported it and included buildflags.mk in d.rules.
    - Do not overwrite CFLAGS anymore, we now use CFLAGS += in d.rules.
  * Enable installation of multiarch binaries :
    - Added Pre-Depends: ${misc:Pre-Depends} for the libfko package in
      d.control.
    - Replaced occurrences of /usr/lib/ in debian/*.install with /usr/lib/*/.
  * Fixed d.rules to handle the new generated configuration files
    [access|fwknopd].conf.inst. We revert upstream changes with an overwrite
    on dh_install.
  * Updating fwknop-server.postinst to disable easy setup.

 -- Franck Joncourt <franck@debian.org>  Sun, 26 Aug 2012 22:45:00 +0200

fwknop (2.0.0rc2-2) sid; urgency=low

  * Acknowledged NMU : 1.9.12-3.1
  * Set Architecture as linux-any (Closes: #647654)
  * Fixed mispelling dependency on the default-mta in d.control.
    (Closes: #645024)
  * The brand new C implementation :
    + does not use Perl anymore. (Closes: #674115)
    + does not use the whatismyip url anymore ; it is replace by the
      cipherdyne url at the time. (Closes: #645994)
  * Bumped up Standards-Version to 3.9.3:
    + Refreshed copyright holders and updated d.copyright to comply with the
      1.0 machine-readable copyright file specification.
  * Updated init script to use log_daemon_msg and log_end_msg function from
    the lsb library.
  * Avoid debconf error if not available. (Closes: #673773)

 -- Franck Joncourt <franck@debian.org>  Sun, 24 Jun 2012 20:57:59 +0200

fwknop (2.0.0rc2-1) experimental; urgency=low

  * New upstream release.
    + Drop the Perl implementation which is not shipped anymore.
    + Renamed the C implementation from fwknop-c-{client/server} to
      fwknop-{client/server}.
    + Bumped shared library version up to 0.0.2.
      Refreshed libfko0-dev.links, libfko0.install and libfko0.links
      accordingly.
    + Updated d.rules to not exclude the fwknop-server package when stripping
      the symbols since we have renamed the C version in fwknop-server.
  * Removed Replaces/Conflicts/Provides dependencies for both the
    fwknop-client and fwknop-server in d.control for the old
    fwknop-c-{client/server}. This is not needed anymore.
  * Bumped up Standards-Version to 3.9.1 (no changes).
  * Set copyright and licence information in d.copyright:
    + Added patch licence.patch from upstream to improve copyright statements
      and licence definitions.
  * Added missing debconf support for fwknop-server. Both the
    fwknop-server.postinst and the fwknop-server.postrm are added.
  * Added fwknop-server.preinst to remove old files from the Perl
    implementation (Fwknop 1.9.x).

 -- Franck Joncourt <franck@debian.org>  Sun, 14 Nov 2010 17:12:20 +0100

fwknop (2.0.0rc2~svn281-1) unstable; urgency=low

  * Merge new upstream release
  * Fixed access.conf permission to 600 rather than the default 644 value:
    + Added lintian overrides.
  * Added debian watch.
  * Fixed source url in d.copyright to point to the new place where fwknop is
    stored.
  * Fixed typo in fwknop-c-server.default. The override option is case
    sensitive ans should be written in lower case.

 -- Franck Joncourt <franck@debian.org>  Sat, 14 Aug 2010 13:29:25 +0200

fwknop (2.0.0rc1~svn271-1) unstable; urgency=low

  *  New upstream release.

 -- Franck Joncourt <franck@debian.org>  Sun, 25 Jul 2010 20:55:10 +0200

fwknop (1.9.12-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Danish (Joe Hansen).  Closes: #630373
    - Dutch; (Jeroen Schot).  Closes: #651829
    - Polish. Closes: #659370

 -- Christian Perrier <bubulle@debian.org>  Sun, 19 Feb 2012 07:22:24 +0100

fwknop (1.9.12-2) unstable; urgency=low

  * Refreshed maintainer email address:
    + d.control: Updated email address and removed DM-Upload-Allowed flag.
    + d.copyright: Refreshed email address.
  * initscript:
    + Fixed the Provide target in fwknop-server initscript:
      http://lintian.debian.org/tags/init.d-script-does-not-provide-itself.html
    + Added dependency against lsb-base (>= 3.0-6) to ensure the
      /lib/lsb/init-functions can be sourced through the initscript.
  * Added Spanish debconf translation. (Closes: #582870)
  * Updated Depends field in d.control to use default-mta rather than exim4.
  * Switched to dpkg-source 3.0 (quilt) format.
  * Added t_upstream_spelling.diff patch to fix spelling erros.
  * Bumped up Standards-Version to 3.9.0 (no changes).
  * Refreshed d.copyright to follow the latest DEP5.

 -- Franck Joncourt <franck@debian.org>  Wed, 07 Jul 2010 21:50:16 +0200

fwknop (1.9.12-1) unstable; urgency=low

  * New Upstream Version
  * Removed [code] tags from README.Debian. All code blocks are now indented.
  * Bumped up Standards-Version to 3.8.3 (no change).
  * Updated copyright years in d.changelog.
  * Updated comments in:
      + d.fwknop-server.config
      + d.fwknop-server.default
      + d.README.Debian
  * Included czech debconf translation. (Closes: #535984)
  * d.fwknop-server.config does not pass -e to the shell on the #! line anymore,
    but set it in the body of the script with set -e (Debian Policy 10.4).

 -- Franck Joncourt <franck.mail@dthconnex.com>  Wed, 09 Sep 2009 21:08:42 +0200

fwknop (1.9.11-1) unstable; urgency=low

  * New Upstream Version.
  * Set default value for variables in /etc/default/fwknop-server
    (Closes: #521677)
  * As /var/run can be a temporary filesystem, this one should be created
    when running the initscript.
    http://lintian.debian.org/tags/dir-or-file-in-var-run.html
  * Bumped up Standards-Version to 3.8.1.
    + d.rules: Handle the noopt option through the DEB_BUILD_OPTIONS variable.

 -- Franck Joncourt <franck.mail@dthconnex.com>  Sun, 17 May 2009 11:31:20 +0200

fwknop (1.9.10-2) unstable; urgency=low

   * Added quilt framework along with README.source
   * Added patch to fwknopd in order to bypass #508432.
     Thanks to Martin Ferrari.

 -- Franck Joncourt <franck.mail@dthconnex.com>  Mon, 23 Feb 2009 22:04:56 +0100

fwknop (1.9.10-1) unstable; urgency=low

  * New Upstream Version
  * The sendmail command is now handled by knopwatchd. (Closes: #506819)
  * Fixed "Setting ALERTING_METHODS to noemail reports an error".
    (Closes: #506815)

 -- Franck Joncourt <franck.mail@dthconnex.com>  Wed, 14 Jan 2009 21:11:17 +0100

fwknop (1.9.9-1) unstable; urgency=low

  * New Upstream Version
  * Fixed typo in fwknop-server init-script. (Closes: #504665)
  * Added DAEMON_ARGS variable to allow users to pass specifics options to
    the daemon. (Closes: #504666)
  * Adjusted priority from optional to extra.
    (Fwknop has some dependencies on packages whose priority is extra)
  * Fixed dependency on the mail command. (Closes: #504670)

 -- Franck Joncourt <franck.mail@dthconnex.com>  Sat, 15 Nov 2008 14:44:08 +0100

fwknop (1.9.8-1) unstable; urgency=low

  * Initial release (Closes: #406627)
  * Includes fr debconf translation (Closes: #500655)

 -- Franck Joncourt <franck.mail@dthconnex.com>  Sun, 02 Oct 2008 19:32:55 +0200
